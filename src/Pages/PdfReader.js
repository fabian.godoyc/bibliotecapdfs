 import React,{useState} from 'react'
import {Document, Page}from 'react-pdf/dist/esm/entry.webpack'
import { useLocation,Link } from 'react-router-dom';
import styles from './mystyle.module.css'; 

function Reader(){  
  const[numPages, setNumPages] = useState(null);
  const[pageNumber, setPageNumber] = useState(1);

  const location = useLocation()
  const pdfName = location.state.Name
  
  function onDocumentLoadSuccess({numPages}){
    setNumPages(numPages);
    setPageNumber(1);
  }

  function changePage(offSet){
    setPageNumber(prevPageNumber => prevPageNumber+offSet)
  }

  function changePageBack(){
    changePage(-1)
  }

  function changePageNext(){
    changePage(+1)
  }

  return (
    <div className="Reader">
      <header>
        <Link to="/">
          <h2 className={styles.backlink}>Regresar a Biblioteca</h2>
        </Link>
      </header>
      <center className="App-header">
        <Document file={"pdfFolder/"+pdfName.key} onLoadSuccess={onDocumentLoadSuccess}>
          <Page height="1080" pageNumber={pageNumber}/>
        </Document>
        <p>Page {pageNumber} of {numPages}</p>
        { pageNumber >1 &&
        <button onClick={changePageBack}>Previous Page</button>
        }
        { pageNumber <numPages &&
        <button onClick={changePageNext}>Next Page</button>
        }
      </center>
      
    </div>
  );
}
export default Reader