import{Link} from "react-router-dom"
import data from "../pdfs.json";
import styles from './mystyle.module.css'; 
function MainPage(){
    return(
    <div className="MainPage">
        <header>
            <h1 className={styles.titlestyle}>Biblioteca PDFs</h1>
        </header>
        <center>
            {Object.keys(data).map((key,i) => (
                <Link to="/pdf" state={{Name:{key}}} >
                    <h2 className={styles.linkpdf}>{data[key]}</h2>
                </Link>
            ))}
        </center>   
    </div>)
}
export default MainPage
