import {BrowserRouter, Routes, Route} from "react-router-dom";
import Reader from "./Pages/PdfReader";
import MainPage from "./Pages/MainPage";

function App() {

  return (
    //
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route exact path="/"  element={<MainPage/>}/>
          <Route exact path="/pdf"  element={<Reader/>}/>
        </Routes>
      </div>
    </BrowserRouter>
    
  );
}

export default App;